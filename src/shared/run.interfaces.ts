export interface TimeData {
  billable: boolean,
  client: string,
  description: string,
  hourDuration: number,
  project: string,
}

export type Report = TimeData[]
