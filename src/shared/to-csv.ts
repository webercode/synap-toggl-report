import { parse } from 'json2csv';
import { Report, TimeData } from './run.interfaces';

export function toCsv(report: Report): string {

  return parse<TimeData>(report, {
    fields: [
      {
        label: 'Client',
        value: 'client',
      },
      {
        label: 'Project',
        value: 'project',
      },
      {
        label: 'Description',
        value: 'description',
      },
      {
        label: 'Billable',
        value: 'billable',
      },
      {
        label: 'Duration (hr)',
        value: (row: TimeData) => {
          const hrs = Math.trunc(row.hourDuration);
          const mins = Math.round((row.hourDuration - hrs) * 60);
          return `${hrs}:${mins.toString().padStart(2, '0')}`;
        },
      },
      {
        label: 'Duration (format as [HH]:MM in Excel)',
        value: (row: TimeData) => row.hourDuration / 24,
      },
    ],
  });
}
