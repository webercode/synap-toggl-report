import { inspect } from 'util';
import { Report } from './run.interfaces';
import { TogglClientTypes } from './toggl-client.interfaces';

export function generateReport(togglReport: TogglClientTypes.DetailedReport): Report {

  const report: Report = [];

  togglReport.data.forEach(timeEntry => timeEntry.is_billable ?
    updateBillable(timeEntry, report) :
    updateNonBillable(timeEntry, report));

  return report;
}

function findOrCreate<T>(array: T[], predicate: (value: T, index: number, obj: T[]) => unknown, create: () => T): T {

  let value = array.find(predicate);

  if (!value) {
    value = create();
    array.push(value);

  }
  return value;
}

function millisToHours(millis: number) {
  return millis / 1000 / 60 / 60;
}

function updateBillable(timeEntry: TogglClientTypes.TimeEntry, report: Report) {

  validateTags(timeEntry);

  const { client, dur, project, tags } = timeEntry;

  timeEntry.tags.forEach(tag => {

    const entry = findOrCreate(
      report,
      e => e.billable &&
        e.client === client &&
        e.project === project &&
        e.description === tag,
      () => ({ client, project, description: tag, billable: true, hourDuration: 0 }),
    );

    entry.hourDuration += millisToHours(dur / tags.length);
  });
}

function updateNonBillable(timeEntry: TogglClientTypes.TimeEntry, report: Report) {

  const { client, description, dur, project } = timeEntry;

  const nonBillable = findOrCreate(
    report,
    e => !e.billable &&
      e.client === client &&
      e.project === project &&
      e.description === description,
    () => ({ client, project, description, billable: false, hourDuration: 0 }),
  );

  nonBillable.hourDuration += millisToHours(dur);
}

function validateTags(timeEntry: TogglClientTypes.TimeEntry) {

  const numTags = timeEntry.tags.length;

  if (numTags === 0) {
    const tag = 'Unknown';
    console.warn(`Time entry has no tag! Setting tag to "${tag}".`);
    console.warn(inspect(timeEntry, { breakLength: Infinity }));
    timeEntry.tags = [tag];

  } else if (numTags > 1) {
    console.warn('Time entry has multiple tags! Time will be split evenly between tags.');
    console.warn(inspect(timeEntry, { breakLength: Infinity }));
  }
}
