import { promisify } from 'util';
import { AuthResult } from './inquirer.interfaces';
import { TogglClientTypes } from './toggl-client.interfaces';

const TogglApiClient = require('toggl-api');

export class TogglClient {

  private toggl = new TogglApiClient(this.input);
  private _getUserData = promisify(this.toggl.getUserData).bind(this.toggl);
  private _getWorkspaceClients = promisify(this.toggl.getWorkspaceClients).bind(this.toggl);
  private _detailedReport = promisify(this.toggl.detailedReport).bind(this.toggl);

  constructor(private input: AuthResult) {}

  detailedReport(opts: { [i: string]: any }): Promise<TogglClientTypes.DetailedReport> {
    return this._detailedReport(opts);
  }

  async detailedReportFull(opts: { [i: string]: any }): Promise<TogglClientTypes.DetailedReport> {

    const report = await this.detailedReport(opts);

    if (report.total_count > report.per_page) {
      const pages = Math.ceil(report.total_count / report.per_page);
      for (let i = 2; i <= pages; i++) {
        const next = await this.detailedReport(Object.assign({}, opts, { page: i }));
        report.data = report.data.concat(next.data);
      }
    }

    return report;
  }

  getUserData(options: any = {}): Promise<TogglClientTypes.UserData> {
    return this._getUserData(options);
  }

  getWorkspaceClients(wId: string|number): Promise<TogglClientTypes.ClientData[]> {
    return this._getWorkspaceClients(wId);
  }

}
