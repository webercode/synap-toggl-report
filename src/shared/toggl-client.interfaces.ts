export namespace TogglClientTypes {

  export interface ClientData {
    at: string
    id: number,
    name: string,
    wid: number,
  }

  export interface DetailedReport {
    data: TimeEntry[],
    per_page: number,
    total_billable: number,
    total_count: number,
    total_grand: number,
  }

  export interface TimeEntry {
    client: string,
    description: string,
    dur: number,
    end: string,
    is_billable: boolean,
    project: string,
    start: string,
    tags: string[]
  }

  export interface UserData {
    default_wid: number,
    email: string,
    fullname: string,
    id: number,
    workspaces: WorkspaceData[],
  }

  export interface WorkspaceData {
    id: number,
    name: string,
  }
}
