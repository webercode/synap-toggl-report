export type AuthResult = AuthBasicResult|AuthTokenResult

interface AuthTokenResult {
  apiToken: string,
  auth: 'token',
}

interface AuthBasicResult {
  auth: 'basic',
  password: string,
  username: string,
}

export interface ReportOptionsResult {
  from: Date,
  until: Date,
  workspaceName: string,
}

export interface OutputOptionsResult {
  filename: string,
  format: 'csv'|'json',
}
