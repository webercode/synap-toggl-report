import * as inquirer from 'inquirer';
import moment from 'moment';
import { join, parse } from 'path';
import { AuthResult, OutputOptionsResult, ReportOptionsResult } from './inquirer.interfaces';

inquirer.registerPrompt('datepicker', require('inquirer-datepicker'));

export class Inquirer {

  async getAuth(): Promise<AuthResult> {

    return await inquirer.prompt<AuthResult>([
      {
        type: 'list',
        name: 'auth',
        message: 'Choose an auth type',
        choices: [
          { value: 'token' },
          // TODO figure out how to allow basic auth
          { value: 'basic', disabled: true },
        ],
      },
      {
        type: 'input',
        name: 'apiToken',
        message: 'Enter your Toggl API token',
        when: input => input.auth === 'token',
        validate: input => input.length > 0 || 'api token required',
      },
      {
        type: 'input',
        name: 'username',
        message: 'Enter your Toggl username or email',
        when: input => input.auth === 'basic',
        validate: input => input.length > 0 || 'username / email required',
      },
      {
        type: 'password',
        name: 'password',
        message: 'Enter your Toggl password',
        mask: true,
        when: input => input.auth === 'basic',
        validate: input => input.length > 0 || 'password required',
      },
    ]);
  }

  async getOutputOptions(defaultName = 'report'): Promise<OutputOptionsResult> {

    const transformer = (input: string, answers: OutputOptionsResult) => {
      const parts = parse(input);
      return input ? join(parts.root, parts.dir, `${parts.name}.${answers.format}`) : input;
    };

    return await inquirer.prompt<OutputOptionsResult>([
      {
        type: 'list',
        name: 'format',
        message: 'Choose a report format',
        choices: [
          { value: 'csv' },
          { value: 'json' },
        ],
      },
      {
        type: 'input',
        name: 'filename',
        message: 'Enter a name for the report file',
        validate: input => input.length > 0 || 'filename required',
        default: (answers: OutputOptionsResult) => `${defaultName}.${answers.format}`,
        filter: transformer,
        transformer,
      },
    ]);
  }

  async getReportOptions(workspaces: any[]): Promise<ReportOptionsResult> {

    const anchor = moment().startOf('isoWeek').subtract(7, 'days');
    const from = anchor.toDate();

    return await inquirer.prompt<ReportOptionsResult>([
      {
        type: 'list',
        name: 'workspaceName',
        message: 'Select your desired workspace',
        choices: workspaces.map(ws => ws.name),
      },
      {
        type: 'datepicker',
        name: 'from',
        message: 'Select start date of report',
        format: ['Y', '-', 'MM', '-', 'DD', ' (dddd)'],
        default: from,
      },
      {
        type: 'datepicker',
        name: 'until',
        message: 'Select end date of report',
        format: ['Y', '-', 'MM', '-', 'DD', ' (dddd)'],
        default: (answers: ReportOptionsResult) => moment(answers.from).add(6, 'days').toDate(),
      },
    ]);
  }
}
