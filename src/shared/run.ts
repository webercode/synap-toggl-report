import { writeFileSync } from 'fs';
import moment from 'moment';
import { generateReport } from './generate-report';
import { Inquirer } from './inquirer';
import { TimeData } from './run.interfaces';
import { toCsv } from './to-csv';
import { TogglClient } from './toggl-client';

export async function run(): Promise<void> {

  const inquirer = new Inquirer();
  const auth = await inquirer.getAuth();
  const toggl = new TogglClient(auth);
  const user = await toggl.getUserData();
  const { default_wid, workspaces } = user;
  const iDefaultWs = workspaces.findIndex(ws => ws.id === default_wid);

  workspaces[iDefaultWs].name += ' (default)';

  workspaces.sort((a, b) =>
    a.id === default_wid ? -1 : b.id === default_wid ? 1 :
      a.name > b.name ? 1 : a.name < b.name ? -1 : 0);

  const reportOpts = await inquirer.getReportOptions(workspaces);
  const workspace = user.workspaces.find(ws => ws.name === reportOpts.workspaceName);

  if (!workspace) throw new Error('failed to find workspace');

  const since = formatter(reportOpts.from);
  const until = formatter(reportOpts.until);

  const togglReport = await toggl.detailedReportFull({
    workspace_id: workspace.id,
    user_ids: user.id,
    since,
    until,
  });

  const report = generateReport(togglReport).sort(sorter);
  const defaultName = `${formatter(reportOpts.from)}_${formatter(reportOpts.until)}_${user.fullname}`;
  const outputOpts = await inquirer.getOutputOptions(defaultName);
  const data = outputOpts.format === 'json' ? JSON.stringify(report, undefined, 2) : toCsv(report);

  writeFileSync(outputOpts.filename, data);
}

function formatter(date: Date): string {
  return moment(date).format('YYYY-MM-DD');
}

function sortStr(value: TimeData): string {

  return [
    value.billable ? 0 : 1,
    value.client,
    value.project,
    value.description,
  ].join('');
}

function sorter(a: TimeData, b: TimeData): -1|0|1 {

  const aStr = sortStr(a);
  const bStr = sortStr(b);
  return aStr > bStr ? 1 : aStr < bStr ? -1 : 0;
}
