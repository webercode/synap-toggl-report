#!/usr/bin/env node

import { run } from '../shared/run';

(async function main() {
  try {
    await run();
  }
  catch (e) {
    console.error(e);
  }
})();
