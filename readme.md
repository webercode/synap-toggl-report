# Synap Toggl Report

A small CLI utilizing the Toggl API to generate a summary report of billable hours within the indicated
time range. As the reporting needs change, so will the logic and the version.

## Install the Script

Install globally using one of the following methods:

- NPM registry - `npm i -g @webercode/synap-toggl-report`
- BitBucket repo - `npm i -g bitbucket:webercode/synap-toggl-report`

Feel free to use the package manager of your choosing.

## Run the Script

Once installed globally, you can run using the binary `synap-toggl-report` and follow
the prompts to write the report to a CSV file.

Alternatively, to run a single time and immediately uninstall after running, use `npx`:

- NPM registry - `npx -p @webercode/synap-toggl-report synap-toggl-report`
- BitBucket repo - `npx -p bitbucket:webercode/synap-toggl-report synap-toggl-report`

## Explanation of Script Logic

The script uses `inquirer` to prompt for information in two stages:

1. Prompt for authentication
1. Prompt for Workspace and other details

You can authenticate using a token ~~or basic credentials~~ (basic credentials are not currently working
due to an error in `toggl-api`). To get your API token, go to https://toggl.com/app/profile
and at the very bottom you should see an *API token*.

Once authenticated, the script will retrieve your data and then let you choose
the workspace from which to create the report. The start date for the report defaults to the start of Monday
of last week, and the end date for the report defaults to the end of the following Sunday.
You can, of course, change these values.

At the moment, the goal is simply to retrieve a detailed report and output the following CSV file:

```csv
Client,Project,Description,Billable,Duration (hr)
"McClelland Labs, Inc.",Lab Automation,Project Management,true,1:15
Synap,Company Time,Weekly Meeting,false,1:00
```

For billable, the description is the common tag for accumulated time entries.
For non-billable, the description is the common description of the accumulated time-entries.

_**WARNING** - if a billable time entry has no tags, or more than one tag
associated with it, the script will emit a warning to help identify the
offending time entry. However, to allow for flexibility, a generic "Unknown"
tag will be created or the duration of the time entry will be split between
multiple tags; if the time entry is non-billable, this doesn't really matter as
non-billable hours are grouped by description, not tag._
